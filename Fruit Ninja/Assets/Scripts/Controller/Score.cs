using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public ScoreScreen scoreScreen;
    internal int points = 0;
    readonly int pointValue = 1;

    private static Score instance;
    public static Score Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Score>();
            }

            return instance;
        }
    }

    internal void ResetPoints()
    {
        points = 0;
        scoreScreen.UpdateScore(points);
    }

    internal void AddPoints()
    {
        points += pointValue;
        scoreScreen.UpdateScore(points);
    }
}
