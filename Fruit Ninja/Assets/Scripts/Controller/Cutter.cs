using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutter : MonoBehaviour
{
    public bool isCutting = false;
    public GameObject cuttingEffectPrefab;
    GameObject cuttingEffect;
    Rigidbody2D cutterRigidbody;
    CircleCollider2D cutterCollider;
    Camera cam;
    Vector2 lastPosition;
    float minVelocity = 0.0001f;
    float timeToDestroy = 0.1f;

    void Start()
    {
        cutterRigidbody = GetComponent<Rigidbody2D>();
        cutterCollider = GetComponent<CircleCollider2D>();
        cam = Camera.main;
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            StartCut();
        }
        else if(Input.GetMouseButtonUp(0))
        {
            StopCut();
        }

        if (isCutting)
        {
            UpdateCut();
        }
    }

    void StartCut()
    {
        isCutting = true;
        cuttingEffect = Instantiate(cuttingEffectPrefab, transform);
        cutterCollider.enabled = true;
        lastPosition = cam.ScreenToWorldPoint(Input.mousePosition);
    }

    void StopCut()
    {
        isCutting = false;
        Destroy(cuttingEffect, timeToDestroy);
        cutterCollider.enabled = false;
    }

    void UpdateCut()
    {
        Vector2 position = cam.ScreenToWorldPoint(Input.mousePosition);
        cutterRigidbody.position = position;
        Vector2 newPosition = position - lastPosition;
        float velocity = newPosition.magnitude * Time.deltaTime;

        if(velocity > minVelocity)
        {
            cutterCollider.enabled = true;
        }
        else
        {
            cutterCollider.enabled = false;
        }

        lastPosition = position;
    }

}
