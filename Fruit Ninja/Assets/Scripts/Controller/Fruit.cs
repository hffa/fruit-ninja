using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour
{
    [Tooltip("Prefab a ser instanciado")]
    public GameObject slicedFruitPrefab;
    [Tooltip("Tempo para destruir o objeto")]
    public float timeToDestroy = 5;
    [Tooltip("For�a do arremesso")]
    public float force = 13;
    Rigidbody2D fruitRigidbody;
    bool wasSliced = false;


    void Start()
    {
        fruitRigidbody = GetComponent<Rigidbody2D>();
        Vector2 forceVector = transform.up * force;
        fruitRigidbody.AddForce(forceVector, ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Cutter") && !wasSliced)
        {
            wasSliced = true;
            Score.Instance.AddPoints();
            CreateSlicedFruit(collision.transform);
        }
    }

    void CreateSlicedFruit(Transform objTransform)
    {
        Vector2 direction = (objTransform.position - transform.position).normalized;
        Quaternion rotation = Quaternion.LookRotation(direction);
        GameObject slicedFruit = Instantiate(slicedFruitPrefab, transform);
        slicedFruit.transform.rotation = rotation;
        Destroy(slicedFruit, timeToDestroy);
        GetComponent<MeshRenderer>().enabled = false;
        Destroy(gameObject, timeToDestroy);
    }
}
