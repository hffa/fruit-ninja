using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    [Tooltip("Prefab a ser instanciado")]
    public GameObject objPrefab;
    [Tooltip("Pontos de referencia para o spawn")]
    public Transform[] spawnPoints;
    [Tooltip("Tempo minimo entre a cria��o dos objetos")]
    public float minTimeBetweenSpawn = 0.5f;
    [Tooltip("Tempo maximo entre a cria��o dos objetos")]
    public float maxTimeBetweenSpawn = 2;
    [Tooltip("Tempo para destruir o objeto")]
    public float timeToDestroy = 8;

    void Start()
    {
        StartCoroutine(Spawn());
    }

    void SpawnObj()
    {
        int spawnIndex = Random.Range(0, spawnPoints.Length);
        Transform objParent = spawnPoints[spawnIndex];
        GameObject obj = Instantiate(objPrefab, objParent);
        Destroy(obj, timeToDestroy);
    }

    IEnumerator Spawn()
    {
        while (true)
        {
            float timer = Random.Range(minTimeBetweenSpawn, maxTimeBetweenSpawn);
            yield return new  WaitForSeconds(1);
            SpawnObj();
        }
    }
}
